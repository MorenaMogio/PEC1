var myApp = angular.module('ConversorMonedas', []);

myApp.controller('MainController', ['$scope',function($scope) {
	$scope.cantidad = 1.00;
	$scope.monedasOrig = 'EUR';
	$scope.monedasDest = 'EUR';
	$scope.resultado = 1.00;
	console.log('Inicializando el controlador');
	$scope.convierte = function(){
		var cantidad = $scope.cantidad;
		var resultado = 0.00;
		if ($scope.monedasOrig == 'EUR') {
			switch($scope.monedasDest) {
				case 'USD':
					resultado = (cantidad * 1.2);
					break;
				case 'GBP':
					resultado = (cantidad * 0.71);
					break;
				default:
					resultado = cantidad;
			} 
		}
		else {
			if ($scope.monedasOrig == 'USD'){
				switch($scope.monedasDest) {
					case 'EUR':
						resultado = (cantidad * 0.83);
						break;
					case 'GBP':
						resultado = (cantidad * 0.59);
						break;
					default:
						resultado = cantidad;
				} 
			}
			else{
				switch($scope.monedasDest) {
					case 'EUR':
						resultado = (cantidad * 1.4);
						break;
					case 'USD':
						resultado = (cantidad * 1.68);
						break;
					default:
						resultado = cantidad;
				} 	
			}
		}
		$scope.resultado = resultado.toFixed(2);
	};
}]);